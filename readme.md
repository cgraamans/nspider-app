#NSPIDER APP#

The ionic app build for https://www.nspider.net

### requires ###

full ionic 2 install with cordova and the android sdk

### install ###

git pull it, npm install it

### build instructions ###

    ionic cordova build android --prod

### serve instructions ###

    ionic serve -c

### debug it ###

    ./node_modules/.bin/ngc

### party with it? ###

Dev build in the dev branch.

:) <3