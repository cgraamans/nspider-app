import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { ModelDataProvider } from '../providers/model-data/model-data';
import { ServiceSocketProvider } from '../providers/service-socket/service-socket';

import { ItemsPage } from '../pages/items/items';
import { SourcePage } from '../pages/source/source';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage:any = ItemsPage;
  theme:String;
  sources:Array<any> = [];
  itemLogoLocation:string = this.data.assets.images;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,private data:ModelDataProvider,private socket:ServiceSocketProvider) {
    
    this.data.getActiveThemeObservable().subscribe(val=>{
      this.theme = val;
    });
    
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    this.socket.on('sources').subscribe(val=>{
      
      if(val.ok === true){

        this.sources = val.sources;

      }

    });

    this.socket.on('user-save').subscribe(val=>{

      if(val.ok === true) {

        if(val.source) {

          let sourceIdx = this.sources.findIndex(x=>x.stub === val.source);
          if(sourceIdx > -1){

            this.sources[sourceIdx].isFollowed = val.save;

          }

        }

      }
    
    });



  }

  openMenu(){

    this.socket.emit('sources',{language:this.data.getLanguage()});

  }

  openSource(stub){

    this.nav.push(SourcePage,{source:stub});

  }

  filterItems(ev:any){

    let val = ev.target.value;

    if (val && val.trim() !== '') {
      this.sources = this.sources.filter(item=>{
        return item.stub.toLowerCase().includes(val.toLowerCase());
      });
    } else {
      this.socket.emit('sources',{language:this.data.getLanguage()});
    }

  }

}

