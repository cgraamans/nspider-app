import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
const config: SocketIoConfig = { url: 'https://nspider.net', options: {} };

import { MyApp } from './app.component';
import { ItemsPage } from '../pages/items/items';
import { LoginPageModule } from '../pages/login/login.module';
import { PopoverLanguagePageModule } from '../pages/popover-language/popover-language.module';
import { PopoverMenuPageModule } from '../pages/popover-menu/popover-menu.module';
import { PopoverCommentBoxPageModule } from '../pages/popover-comment-box/popover-comment-box.module';
import { RegisterPageModule } from '../pages/register/register.module';
import { SourcePageModule } from '../pages/source/source.module';
import { CommentsPageModule } from '../pages/comments/comments.module';
import { CommentPageModule } from '../pages/comment/comment.module';
import { CommentBoxPageModule } from '../pages/comment-box/comment-box.module';

import { UserItemsPageModule} from '../pages/user-items/user-items.module';
import { UserFollowedPageModule } from '../pages/user-followed/user-followed.module'
import { ComponentsModule } from '../components/components.module';

import { ModelDataProvider } from '../providers/model-data/model-data';
import { ServiceAuthProvider } from '../providers/service-auth/service-auth';
import { ServiceSocketProvider } from '../providers/service-socket/service-socket';

@NgModule({
  declarations: [
    MyApp,
    ItemsPage,
  ],
  imports: [
    BrowserModule,
    SocketIoModule.forRoot(config),
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({name:'nspider'}),
    LoginPageModule,
    RegisterPageModule,
    SourcePageModule,
    CommentsPageModule,
    ComponentsModule,
    PopoverLanguagePageModule,
    PopoverMenuPageModule,
    PopoverCommentBoxPageModule,
    CommentPageModule,
    CommentBoxPageModule,
    UserItemsPageModule,
    UserFollowedPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ItemsPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ModelDataProvider,
    ServiceAuthProvider,
    ServiceSocketProvider,
    InAppBrowser,
  ]
})
export class AppModule {}
