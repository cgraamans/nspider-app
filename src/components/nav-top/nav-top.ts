import { Component } from '@angular/core';
import { PopoverController } from 'ionic-angular';

import { PopoverLanguagePage } from '../../pages/popover-language/popover-language';
import { PopoverMenuPage } from '../../pages/popover-menu/popover-menu';
import { ModelDataProvider } from '../../providers/model-data/model-data';

@Component({
  selector: 'nav-top',
  templateUrl: 'nav-top.html'
})
export class NavTopComponent {

  constructor(private popoverCtrl: PopoverController,private data:ModelDataProvider) {}

  toggleLanguage(evt){

    let popover = this.popoverCtrl.create(PopoverLanguagePage);
    popover.present({
      ev: evt
    });

  }

  toggleMenu(evt){

    let popover = this.popoverCtrl.create(PopoverMenuPage);
    popover.present({
      ev: evt
    });

  }

  toggleTheme(){

  	let activeTheme = this.data.getActiveTheme();
  	if(activeTheme){
  		if(activeTheme === 'dark-theme') {
  			this.data.setActiveTheme('light-theme');
  		} else {
  			this.data.setActiveTheme('dark-theme');
  		}
  	}

  }

  isLoggedIn() {

  	if(this.data.getUser()){
  		return true;
  	} else {
  		return false;
  	}

  }

}
