import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ServiceSocketProvider } from '../../providers/service-socket/service-socket';

@Component({
  selector: 'comment-box',
  templateUrl: 'comment-box.html'
})
export class CommentBoxComponent {

  hide:Boolean = false;

  @Input('txt') txt;
  @Input('item') pItem;
  @Input('comment') pComment;
  @Input('edit') pEdit;

  @Output() submitted = new EventEmitter<number>();

  constructor(private socket:ServiceSocketProvider) {

  }

  public submit() {

  	if(this.txt){
      let emit:{edit?:String,txt?:String,item?:String,comment?:String} = {txt:this.txt};
	  	if(this.pItem){
	  		emit.item = this.pItem;
	  	}
	  	if(this.pComment){
	  		emit.comment = this.pComment;
	  	}
      if(this.pEdit){
        emit.edit = this.pEdit;
      }
	  	this.socket.emit('user-comment',emit);
      this.submitted.emit(1);
    
    }


  }

}
