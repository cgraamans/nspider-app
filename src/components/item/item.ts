import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { SourcePage } from '../../pages/source/source';
import { CommentsPage } from '../../pages/comments/comments';

import { ModelDataProvider } from '../../providers/model-data/model-data';
import { ServiceSocketProvider } from '../../providers/service-socket/service-socket';

/**
 * Generated class for the ItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'item',
  templateUrl: 'item.html'
})
export class ItemComponent {

  @Input('item') item;
  @Input('assets') assets;

  constructor(private iab: InAppBrowser, private navCtrl:NavController, private data:ModelDataProvider, private socket:ServiceSocketProvider) {}

  pushSource(stub){

    this.navCtrl.popToRoot();
  	this.navCtrl.push(SourcePage,{source:stub});

  }

  pushComments(stub){

    this.navCtrl.popToRoot();
  	this.navCtrl.push(CommentsPage,{item:stub});

  }

  link(link){

    this.iab.create(link);

  }

  toggleItemDetails(item) {
  	
  	item._details ? item._details = false : item._details = true;

  }

  dt(unixtime){

  	let elapsed = Math.round((new Date()).getTime()/1000) - unixtime;

  	if(elapsed < 0){

  		return '0 s';

  	}

  	if(elapsed < 60) {

  		return elapsed+' s';

  	}
  	if(elapsed < 3600) {

  		return Math.round((elapsed/60)*10)/10+' m';

  	}

  	if(elapsed < 86400) {

  		return Math.round((elapsed/3600)*10)/10+' h';

  	}

  	if(elapsed >= 86400) {

  		return Math.round((elapsed/86400)*10)/10+' d';

  	}

  }

  countrify(countryCode){
    return countryCode.toLowerCase();
  }

  isLoggedIn(){

    return this.data.getUser()? true : false;

  }

  setSave(){
    let save = {
      item:this.item.stub,
      save:1
    };
    if(this.item.saved === 1){
      save.save = 0;
    }
    this.socket.emit('user-save',save);

  }

}
