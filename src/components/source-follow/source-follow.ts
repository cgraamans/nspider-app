import { Component, Input } from '@angular/core';
import {ModelDataProvider} from '../../providers/model-data/model-data';
import {ServiceSocketProvider} from '../../providers/service-socket/service-socket';

@Component({
  selector: 'source-follow',
  templateUrl: 'source-follow.html'
})
export class SourceFollowComponent {

	@Input('stub') stub;
	@Input('isSaved') isSaved;
	_disabled:boolean = false;

  constructor(private data:ModelDataProvider,private socket:ServiceSocketProvider) {
  	
  	let usr:any = this.data.getUser();
  	if(!usr) this._disabled = true;

  }

  follow(evt) {

  	console.log(this.isSaved);

  	evt.stopPropagation();

  	let emit = {source:this.stub,save:0};
  	if(this.isSaved < 1) emit.save = 1;

  	console.log(emit);

  	this.socket.emit('user-save',emit);

  }

}
