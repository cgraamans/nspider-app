import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Storage } from '@ionic/storage';
import { MenuController } from 'ionic-angular';

@Injectable()
export class ModelDataProvider {

  //
  // Settings
  //

  assets = {

  	images:'http://85.214.229.198/assets/' // image assets for avatar icons
  
  };

  settings = {

  	theme:'light-theme', // starting theme
  	language:'en', // starting language

  };

  //
  // DO NOT EDIT BELOW
  //

  private theme: BehaviorSubject<String> = new BehaviorSubject('light-theme');
  private user:BehaviorSubject<Object> = new BehaviorSubject(null);
  private userLanguage:BehaviorSubject<String> = new BehaviorSubject('en'); 

  constructor(private storage:Storage){

    this.storage.ready().then(() => {

    	this.storage.get('_language').then(val => {

    		if(!val){

    			this.storage.set('_language',this.userLanguage.getValue());

    		} else {

    			this.userLanguage.next(val);

    		}

    	});

    	this.storage.get('_theme').then(val => {

    		if(!val){
          
          
            this.storage.set('_theme',this.theme.getValue());
          

    		} else {

    			this.theme.next(val);

    		}

    	});
      
    });
    

  }

  setLanguage(lang:String) {

    this.storage.ready().then(() => {

  	  this.storage.set('_language',lang);

    });
  	this.userLanguage.next(lang);

  }

  getLanguage() {

    return this.userLanguage.getValue();

  }

  getLanguageObservable() {

    return this.userLanguage.asObservable();

  }

  login(apiId,token,auth,name){

  	this.user.next({
  		auth:auth,
  		apiId:apiId,
  		token:token,
  		name:name	
  	});

    this.storage.ready().then(() => {

      this.storage.set('_user',this.user.getValue());

    });

  }

  logout(){

    this.user.next(null);

    this.storage.ready().then(() => {

      this.storage.remove('_user');

    });

  }

  setActiveTheme(val) {

    this.storage.ready().then(() => {

      this.storage.set('_theme',val);

    });
    this.theme.next(val);

  }

  getActiveThemeObservable() {

    return this.theme.asObservable();

  }

  getActiveTheme() {

    return this.theme.getValue();

  }

  getUserObservable() {

    return this.user.asObservable();

  }

  getUser() {

    return this.user.getValue();

  }

}