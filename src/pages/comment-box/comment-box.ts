import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-comment-box',
  templateUrl: 'comment-box.html',
})
export class CommentBoxPage {

  params = {
    item:null,
    txt:null,
    comment:null,
    edit:null
  };

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    if (this.navParams.get('txt')){
      this.params.txt = this.navParams.get('txt');
    }

    if (this.navParams.get('item')){
      this.params.item = this.navParams.get('item');
    }

    if (this.navParams.get('comment')){
      this.params.comment = this.navParams.get('comment');
    }

    if (this.navParams.get('edit')){
      this.params.edit = this.navParams.get('edit');
    }


    if(navParams.data){
      this.params = navParams.data;
    }

  }


  onSubmit(evt){

    if(evt>0){

      this.navCtrl.pop();  
    
    }
    
  }


}
