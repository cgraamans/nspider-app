import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommentBoxPage } from './comment-box';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    CommentBoxPage,
  ],
  imports: [
    IonicPageModule.forChild(CommentBoxPage),
    ComponentsModule
  ],
})
export class CommentBoxPageModule {}
