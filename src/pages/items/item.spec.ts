import {} from 'jasmine';

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { ItemsPage } from "./items";
import { IonicModule, NavController, NavParams } from "ionic-angular/index";

import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
const config: SocketIoConfig = { url: 'http://192.168.178.248:9002', options: {} };
import { IonicStorageModule } from '@ionic/storage';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import * as jwt from 'jsonwebtoken';
import { ServiceAuthProvider } from '../../providers/service-auth/service-auth';
import { ServiceSocketProvider } from '../../providers/service-socket/service-socket';
import { ModelDataProvider } from '../../providers/model-data/model-data';

import { ActionSheetController } from 'ionic-angular';

describe('Items', function () {
  let titleEl: DebugElement;
  let comp: ItemsPage;
  let fixture: ComponentFixture<ItemsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsPage ],
      imports: [
        SocketIoModule.forRoot(config),      
        IonicModule.forRoot(ItemsPage),
        IonicStorageModule.forRoot()
      ],
      providers: [
        NavController,
        { provide: NavParams, useClass: class { NavParams = jasmine.createSpy("NavParams"); }},
        ServiceSocketProvider,
        ServiceAuthProvider,
        InAppBrowser,
        ModelDataProvider,
        { provide: BehaviorSubject, useClass: class { BehaviorSubject = jasmine.createSpy("BehaviorSubject"); }},        
        ActionSheetController
      ]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsPage);
    comp = fixture.componentInstance;
  });

  it('should create component', () => expect(comp).toBeDefined() );

  it('should display the title of the page correctly', () => {
    titleEl = fixture.debugElement.query(By.css('.logo-text'));
    fixture.detectChanges();
    
    const title = titleEl.nativeElement;
    expect(title.innerText).toMatch(/nspider/i,
      '<ionic-title> should display the title of the page correctly"');
  });
});