import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { ServiceSocketProvider } from '../../providers/service-socket/service-socket';
import { ModelDataProvider } from '../../providers/model-data/model-data';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  credentials = {
  	name:'',
  	password:'',
  };

  constructor(public navCtrl: NavController, private socket:ServiceSocketProvider,private data:ModelDataProvider,private viewCtrl:ViewController) {

	this.data.getUserObservable().subscribe(val=>{

		if(val){

			this.viewCtrl.dismiss();

		}

	});

  }

  public register() {

  	this.viewCtrl.dismiss();
  	this.navCtrl.push(RegisterPage);
  
  }

  public login(){

  	if((this.credentials.name.length>0) && (this.credentials.password.length>0)) {

  		this.socket.emit('auth',this.credentials);

  	}

  }

}
