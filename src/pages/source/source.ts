import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Refresher } from 'ionic-angular';
import { ServiceSocketProvider } from '../../providers/service-socket/service-socket';
import { ModelDataProvider } from '../../providers/model-data/model-data';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@IonicPage()
@Component({
  selector: 'page-source',
  templateUrl: 'source.html',
})
export class SourcePage {

  sourceStub:string = null;
  source:{isFollowed?:number,stub?:string} = {};
  loading:boolean = false;
  category:BehaviorSubject<String> = new BehaviorSubject('likes');

  refresher:Refresher = null;
  infiniteScroller:any = null;
  crc = Math.random().toString(36).replace(/[^a-z]+/g, '');
  subscriptions:{source:any,items:any,userVote:any,userSave:any,user:any} = {
    source:null,
    items:null,
    userVote:null,
    user:null,
    userSave:null,
  };

  public isLoggedIn:Boolean = false;
  public items = [];  
  public assetsLocation = this.data.assets.images;

  constructor(public navCtrl: NavController, public navParams: NavParams, public socket:ServiceSocketProvider, public data:ModelDataProvider, public events:Events) {

  	this.sourceStub = this.navParams.get('source');
  	if(this.sourceStub){

  		this.category.subscribe(val=>{

  			this.socket.emit('source',{
  				source:this.sourceStub
  			});
  			this.socket.emit('items',{
  				source:this.sourceStub,
  				category:val,
          _crc:this.crc,
  				limit:20
  			});

  		});

  	} else {

  		this.navCtrl.pop();
    	
  	}

  	this.subscriptions.user = this.data.getUserObservable().subscribe(val=>{

  		if(val){

  			this.isLoggedIn = true;

  		}

  	});

  	this.subscriptions.items = this.socket.on('items').subscribe(val=>{

      if(val.ok === true) {

        if(val._crc){

          if(val._crc === this.crc){

            if (this.refresher) {

              this.items = [];
              this.refresher.complete();
              this.refresher = null;

            }

            if(this.infiniteScroller) {

              this.infiniteScroller.complete();
              this.infiniteScroller = null;

            }

            if(val.items.length>0){

              if(this.items.length>0){

                for(let newItem of val.items){

                  let found = this.items.find(x => x.stub === newItem.stub);
                  if(!found){
                    this.items.push(newItem);
                  }
                }

            } else {

              this.items = val.items;  

            }
            let cat = this.category.getValue();
            switch(cat){
              case 'likes':
                this.items = this.items.sort(this.fieldSorter(['-votes','-_dt']));  
                  break;
              case 'dislikes':
                this.items = this.items.sort(this.fieldSorter(['votes','-_dt']));  
                  break;
              case 'new':
                this.items = this.items.sort(this.fieldSorter(['-_dt']));  
                  break;
              case 'old':
                this.items = this.items.sort(this.fieldSorter(['_dt']));  
                  break;
              case 'comments':
                this.items = this.items.sort(this.fieldSorter(['-comments','-_dt']));  
                  break;                 
               }

            }

          }

        }

      }

  	});

  	this.subscriptions.source = this.socket.on('source').subscribe(val=>{

  		if(val.ok === true) {

  			this.source = val.source;
  		}

  	});

  	this.subscriptions.userVote = this.socket.on('user-vote').subscribe(val=>{

  		if(val.ok=== true){
  			if((val.item) && (this.items.length>0)){

  				let found = this.items.find(x=>x.stub === val.item);
  				if(found){
  					found.vote = val.vote;
  					found.votes = val.votes;
  				}

  			}

  		}

  	});

    this.subscriptions.userSave = this.socket.on('user-save').subscribe(val=>{

      if(val.ok === true) {

      if(!val.save){
        val.save = 0;
      }

        if(val.source){

          this.source.isFollowed = val.save;

        }

        if((val.item) && (this.items.length > 0)){

          let foundIndex = this.items.findIndex(x=>x.stub === val.item);
          if(foundIndex > -1){

            this.items[foundIndex].saved = val.save;

          }

        }

      }

    });

  }

  private fieldSorter(fields) {

	  var dir = [], i, l = fields.length;
	  fields = fields.map(function(o, i) {
	    if (o[0] === "-") {
	      dir[i] = -1;
	      o = o.substring(1);
	    } else {
	      dir[i] = 1;
	    }
	    return o;

	  });

      return function (a, b) {
        for (i = 0; i < l; i++) {
          var o = fields[i];
          if (a[o] > b[o]) return dir[i];
          if (a[o] < b[o]) return -(dir[i]);
        }
        return 0;
      };

  }

  setCategory(category){

  	this.items = [];
  	this.category.next(category._value);

  }

  follow() {

  	let emit = {source:this.source.stub,save:0};
  	if(this.source.isFollowed === 0) emit.save = 1;
  	this.socket.emit('user-save',emit);

  }

  ionViewWillLeave(){

    for(let subKey in this.subscriptions) {

      if(this.subscriptions[subKey] !== null){
        this.subscriptions[subKey].unsubscribe();
      }

    }

  }

  getItemsFrom(evt){

    if(this.items.length > 0){

      let lastItem = this.items[this.items.length-1];
      if(lastItem){

        this.socket.emit('items',{
          category:this.category.getValue(),
          source:this.source.stub,
          from:lastItem.stub,
          _crc:this.crc
        });

      }
      this.infiniteScroller = evt;

    }

  }  

  refresh(refresher:Refresher) {

    this.refresher = refresher;
    this.socket.emit('items',{
      source:this.source.stub,
      category:this.category.getValue(),
      _crc:this.crc    
    });

  }

}
