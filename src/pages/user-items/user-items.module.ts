import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserItemsPage } from './user-items';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    UserItemsPage,
  ],
  imports: [
    IonicPageModule.forChild(UserItemsPage),
    ComponentsModule
  ],
})
export class UserItemsPageModule {}
