import { Component } from '@angular/core';
import { IonicPage, NavController, Refresher, Events } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/observable/combineLatest';

import { ServiceSocketProvider } from '../../providers/service-socket/service-socket';
import { ModelDataProvider } from '../../providers/model-data/model-data';

import {ItemsPage} from '../items/items';


@IonicPage()
@Component({
  selector: 'page-user-items',
  templateUrl: 'user-items.html',
})
export class UserItemsPage {

  items:Array<{stub:string,votes:number,vote?:number,saved?:number}> = [];

  category:BehaviorSubject<string> = new BehaviorSubject(null);
  lang:string;
  loaded:boolean = false;
  refresher:Refresher = null;
  infiniteScroller:any = null;
  assetsLocation:string = null;
  crc = Math.random().toString(36).replace(/[^a-z]+/g, '');

  constructor(public navCtrl:NavController, private socket:ServiceSocketProvider, private data:ModelDataProvider,public events: Events) {

  	let usrCheck = this.data.getUser();
  	if(!usrCheck){
  		this.navCtrl.popToRoot();
  	}

  	this.assetsLocation = this.data.assets.images;
    Observable.combineLatest(this.data.getLanguageObservable(),this.category)
    	.subscribe(r=>{

    		let usr:any = this.data.getUser();
    		this.items = [];
    		if(r.length === 2) {
          		
          		if((r[0]) && (r[1])){

		            let emission:any = {
		              language:r[0],
		              category:r[1],
		              _crc:this.crc,
		              savedBy:usr.name
		            };

    				this.socket.emit('items',emission);

          		}
	    	}

    	});

    this.socket.on('items').subscribe(val=>{

      if(val.ok === true) {

        if(val._crc){

          if(val._crc === this.crc){

      			if (this.refresher) {

      				this.items = [];
      				this.refresher.complete();
      				this.refresher = null;

      			}

      			if(this.infiniteScroller) {

      				this.infiniteScroller.complete();
      				this.infiniteScroller = null;

      			}

      	    if(val.items.length>0){

      	    	if(this.items.length>0){

      	    		for(let newItem of val.items){

      	    			let found = this.items.findIndex(x => x.stub === newItem.stub);
      	    			if(found < 0){
      	    			
                  	this.items.push(newItem);
      	    			
                  } else {

                    this.items[found] = newItem;

                  }

      	    		}

            } else {

            	this.items = val.items;	

            }
            let cat = this.category.getValue();
            switch(cat){
            	case 'likes':
            		this.items = this.items.sort(this.fieldSorter(['-votes','-_dt']));	
            			break;
            	case 'dislikes':
            		this.items = this.items.sort(this.fieldSorter(['votes','-_dt']));	
            			break;
            	case 'new':
            		this.items = this.items.sort(this.fieldSorter(['-_dt']));	
            			break;
            	case 'old':
            		this.items = this.items.sort(this.fieldSorter(['_dt']));	
            			break;
            	case 'comments':
            		this.items = this.items.sort(this.fieldSorter(['-comments','-_dt']));	
            			break;    	   				
             	}
          	    	

          	}
            this.loaded = true;

          }

        }

    	}

    });

    this.socket.on('user-vote').subscribe(val=>{

    	if(val.ok=== true){
    		if((val.item) && (this.items.length>0)){

    			let found = this.items.findIndex(x=>x.stub === val.item);
    			if(found > -1){

    				this.items[found].vote = val.vote;
    				this.items[found].votes = val.votes;

    			}

    		}

    	}

    });

    this.socket.on('user-save').subscribe(val=>{

      if(val.ok === true){

        if(val.item) {

          let found = this.items.findIndex(x=>x.stub === val.item);
          if(found > -1){

            this.items[found].saved = val.save;

          }

        }

      }

    });

    this.category.next('likes');

  } // constructor

  pop() {

  	this.navCtrl.setRoot(ItemsPage);

  }

  setCategory(category){

  	this.loaded = false;
  	this.category.next(category._value);

  }


  getItemsFrom(evt){

  	if(this.items.length > 0){

  		let lastItem = this.items[this.items.length-1];
  		if(lastItem){

  			let usr:any = this.data.getUser();
  			this.socket.emit('items',{
  				category:this.category.getValue(),
  				language:this.data.getLanguage(),
  				from:lastItem.stub,
				_crc:this.crc,
	          	savedBy:usr.name

  			});

  		}
		this.infiniteScroller = evt;

  	}

  }

  private fieldSorter(fields) {

    var dir = [], i, l = fields.length;
    fields = fields.map(function(o, i) {
      if (o[0] === "-") {
        dir[i] = -1;
        o = o.substring(1);
      } else {
        dir[i] = 1;
      }
      return o;

    });

      return function (a, b) {
        for (i = 0; i < l; i++) {
          var o = fields[i];
          if (a[o] > b[o]) return dir[i];
          if (a[o] < b[o]) return -(dir[i]);
        }
        return 0;
      };

  }

  refresh(refresher:Refresher) {

    this.refresher = refresher;
    let usr:any = this.data.getUser();

    this.socket.emit('items',{
      language:this.data.getLanguage(),
      category:this.category.getValue(),
      _crc:this.crc,
      savedBy:usr.name          
    });
    this.loaded = false;

  }

  isLoggedIn() {

    return this.data.getUser()? true : false;
  
  }

}
