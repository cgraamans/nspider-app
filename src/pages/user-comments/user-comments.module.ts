import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserCommentsPage } from './user-comments';

@NgModule({
  declarations: [
    UserCommentsPage,
  ],
  imports: [
    IonicPageModule.forChild(UserCommentsPage),
  ],
})
export class UserCommentsPageModule {}
