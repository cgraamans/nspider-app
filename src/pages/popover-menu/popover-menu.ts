import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { ModelDataProvider } from '../../providers/model-data/model-data';

import { LoginPage } from '../login/login';
import { RegisterPage } from '../register/register';
import { UserItemsPage } from '../user-items/user-items';
import { UserFollowedPage } from '../user-followed/user-followed';



@IonicPage()
@Component({
  selector: 'page-popover-menu',
  templateUrl: 'popover-menu.html',
})
export class PopoverMenuPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public data:ModelDataProvider,private viewCtrl:ViewController) {}

  pop(){

  	this.navCtrl.popToRoot();
  	this.viewCtrl.dismiss();

  }

  pushPage(page){

  	if(page === 'login') {

  		this.navCtrl.push(LoginPage);

  	}

  	if(page === 'register') {

  		this.navCtrl.push(RegisterPage);

  	}

    if(page === 'user-items'){

      this.navCtrl.push(UserItemsPage);
    
    }
    
    if(page === 'user-sources'){

      this.navCtrl.push(UserFollowedPage);
    
    }
    
  	this.viewCtrl.dismiss();

  }

  logout() {

  	this.data.logout();
  	this.viewCtrl.dismiss();

  }

  toggleTheme(){

  	let activeTheme = this.data.getActiveTheme();
  	if(activeTheme){
  		if(activeTheme === 'dark-theme') {
  			this.data.setActiveTheme('light-theme');
  		} else {
  			this.data.setActiveTheme('dark-theme');
  		}
  	}

  }

  isLoggedIn() {

  	if(this.data.getUser()){
  		return true;
  	} else {
  		return false;
  	}

  }


}
