import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PopoverCommentBoxPage } from './popover-comment-box';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    PopoverCommentBoxPage,
  ],
  imports: [
    IonicPageModule.forChild(PopoverCommentBoxPage),
    ComponentsModule
  ],
})
export class PopoverCommentBoxPageModule {}
